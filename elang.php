<?php

class Elang 
{
  public $name = "elang";
  public $jumlahKaki = 2;
  public $keahlian = "terbang tinggi";
  public $attackPower = 10;
  public $deffencePower = 5;
  
  public function getName() {
  	return $this ->name;
  }

  public function getInfo() {
  	return $this->name." ".$this->jumlahKaki." ".$this->keahlian." ".$this->attackPower." ".$this->deffencePower;
  }
  
}

?>